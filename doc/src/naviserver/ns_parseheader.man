[include version_include.man]
[manpage_begin ns_parseheader n [vset version]]
[moddesc {NaviServer Built-in Commands}]

[titledesc {Parse HTTP header}]

[description]
This function parses the HTTP header specified by header into an ns_set specified by set.

[para]
 Optional argument [arg disposition] can be one of the following
 [list_begin itemized]
	[item] toupper - convert header names to upper case
	[item] tolower - convert header names to lower case
	[item] preserve - preserve the existing case
 [list_end]

[section {COMMANDS}]

[list_begin definitions]

[call [cmd ns_parseheader] [arg set] [arg header] \
        [opt  [arg disposition]] ]

[list_end]

[section EXAMPLES]

[example_begin]
[example_end]


[see_also nsd ns_parseurl ns_parsefieldvalue]
[keywords "global built-in" NaviServer parse]
[manpage_end]


