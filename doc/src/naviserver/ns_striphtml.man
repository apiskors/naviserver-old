[include version_include.man]
[manpage_begin ns_striphtml n [vset version]]
[moddesc {NaviServer Built-in Commands}]

[titledesc {Remove HTML tags from a string}]

[description]

 Returns the contents of html with any HTML tags removed.
 This function replaces as well all known HTML4 named entities and
 three digit numeric entities by its UTF-8 representations and removes
 HTML comments.

[section {COMMANDS}]

[list_begin definitions]


[call [cmd ns_striphtml] [arg html]]


[list_end]


[section EXAMPLES]

[example_begin]
   % ns_striphtml "<MARQUEE direction='right'><BLINK>Hello World!</BLINK></MARQUEE>"
   Hello World!
[example_end]


[keywords "global built-in" HTML]

[manpage_end]

