[include version_include.man]

[manpage_begin nsssl n [vset version]]
[moddesc   {NaviServer Modules}]
[titledesc {Configuring https socket communications}]

[description]

The driver module [term nsssl] is used for the socket communication
over HTTPS. The module shares the configuration parameters of
[term nssock] and adds additional parameters. This driver requires a
NaviServer installation with the configuration option --with-openssl
enabled.

[section CONFIGURATION]

This module support all configuration options of [term nssock] module,
plus in addition the following configuration options:

[list_begin definitions]

[def certificate]
is a required parameter, nsssl won't load without it.  The parameter
points to a file containing certificates, which must be in PEM format
and must be sorted starting with the subject's certificate (actual
client or server certificate), followed by intermediate CA
certificates if applicable, and ending at the highest level (root) CA.
The PEM file cem can contain DH parameters (see the example below how
to add these).

[def ciphers]
defines which ciphers will be used. The
ciphers are defined in the OpenSSL "CIPHER LIST FORMAT"
[uri https://www.openssl.org/docs/manmaster/apps/ciphers.html].
By default nsssl uses all ciphers; recommended cipher
suites are published on various sources, such as e.g.:
[uri https://wiki.mozilla.org/Security/Server_Side_TLS]

[def protocols]
defines which protocols are enabled; by default all protocols are
enabled. It is recommended to deactivate SSLv2 and SSLv3 as shown
in the example above.

[def verify]
specifies, whether nsssl should send a client certificate request to
the client. The certificate returned (if any) is checked. If the
verification process fails, the TLS/SSL handshake is immediately
terminated with an alert message containing the reason for the
verification failure.

[def extraheaders]
can be used to specify additional header fields be sent on every
request handled by this driver. The example above,
HTTP Strict Transport Security (HSTS) is enabled.

[list_end]

[section EXAMPLES]

The module is typically loaded per server (specified
below in the variable "server"):

[example_begin]
   ns_section    ns/server/${server}/modules {
      ns_param      nsssl            nsssl.so
   }
   ns_section    ns/server/${server}/module/nsssl {
      ns_param   certificate   /usr/local/ns/modules/nsssl/server.pem
      ns_param   address       0.0.0.0
      ns_param   port          443
      ns_param   ciphers      "ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+3DES:DH+3DES:RSA+AESGCM:RSA+AES:RSA+3DES:!aNULL:!MD5:!RC4"
      ns_param   protocols    "!SSLv2:!SSLv3"
      ns_param   verify         0
    
      ns_param   extraheaders {
         Strict-Transport-Security "max-age=31536000; includeSubDomains"
         X-Frame-Options SAMEORIGIN
         X-Content-Type-Options nosniff
      }
   }
[example_end]

This amount of configuration is sufficient for many installations, but
often one needs different security setting (path the to certificate, port,
ciphers, etc.) or additional settings from [term nssock] such as e.g.
[term writerthreads], [term maxinput] or [term maxupload].

[para]
Below is an example, how a web site can create a self-signed
certificate in PEM format.  The last line with the DH parameters is
optional but necessary perfect forward secrecy.

[example_begin]
    openssl req -new -x509 -sha256 -newkey rsa:2048 -days 365 -nodes  -keyout host.key.pem -out host.cert.pem
    cat host.cert.pem host.key.pem > server.pem
    rm host.cert.pem host.key.pem
    openssl dhparam 2048 >> server.pem
[example_end]


[see_also ns_log ns_write ns_http]
[keywords module nsssl \
	performance tuning configuration TCP_FASTOPEN HTTPS driver]

[manpage_end]
