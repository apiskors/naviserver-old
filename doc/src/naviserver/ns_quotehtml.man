[include version_include.man]
[manpage_begin ns_quotehtml n [vset version]]
[moddesc {NaviServer Built-in Commands}]

[titledesc {Escape HTML characters so they appear as-is in HTML pages}]

[description]

Returns the contents of HTML with certain characters that are special in HTML
replaced with an escape code.  The resulting text can be literally displayed
in a webpage with an HTML renderer. Specifically:

[list_begin definitions]
[def] & becomes &amp;
[def] < becomes &lt;
[def] > becomes &gt;
[def] ' becomes &#39;
[def] " becomes &#34;
[list_end]

 All other characters are unmodified in the output.

[section {COMMANDS}]

[list_begin definitions]

 [call [cmd ns_quotehtml] [arg html]]

[list_end]


[section EXAMPLES]

[example_begin]
   % ns_quotehtml "Hello World!"
   Hello World!
[example_end]

[example_begin]
   % ns_quotehtml "The <STRONG> tag is used to indicate strongly emphasized text."
   The &lt;STRONG&gt; tag is used to indicate strongly emphasized text.
[example_end]

[example_begin]
   % ns_quotehtml {<span class="foo">}
   &lt;span class=&#34;foo&#34;&gt;
[example_end]

[para]
NOTES

 The set of characters that are substituted and their replacements may
 be different in previous versions of NaviServer.  For example,
 NaviServer 2.x did not escape single or double-quotes.

[keywords "global built-in" HTML encoding]


[manpage_end]

