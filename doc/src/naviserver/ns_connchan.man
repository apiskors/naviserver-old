[include version_include.man]
[manpage_begin ns_connchan n [vset version]]
[moddesc {NaviServer Built-in Commands}]

[titledesc {Manage connection channels.}]

[description] The command [cmd ns_connchan] allows one to detach the
 current channel from a connection thread and manage the connection
 outside the initiating connection thread. It allows one to write or read
 to the channel, to define callbacks and to list open connections and
 to close the connection. The read and write operations on this
 channel will use directly the driver infrastructure which was in use
 during the detach command.

[para]
 The command allows e.g. to read from and to write to all
 network drivers (such as plain HTTP channels and from SSL/TLS
 connections). It can be used to implement e.g. WebSockets or
 asynchronous deliveries (e.g. h264 streams) including secure
 connections. Therefore, this command is more general than the
 approaches based on [cmd "ns_conn channel"] using plain Tcl channels.

[para]
 NaviServer maintains an internal table per server to keep track of
 the detached connection channels and to offer introspection to the
 state of the detached channels.

[section {COMMANDS}]

[list_begin definitions]
[call [cmd "ns_connchan detach"]]

 The command [cmd "ns_connchan detach"] unplugs the connection channel from
 the current connection thread and stores it with a fresh handle name
 in a per-virtual-server private table. The command returns the
 created handle as result.

[para]
 After this command was issued in a connection thread all attempts to
 access the connection socket directly (e.g. via [cmd ns_write]) will fail.


[call [cmd "ns_connchan close"] [arg channel]]

 Close the named connection channel.

[call [cmd "ns_connchan exists"] [arg channel]]

 Returns 1 if the named connection channel exists, 0 otherwise.

[call [cmd "ns_connchan list"] \
	[opt [option "-server [arg server]"]] ]

  Return a list of the currently detached connection channels for the
  current or named [arg server].

[para]
 Every list entry contains
 [list_begin itemized]
	[item] name of the channel
	[item] name of the thread
	[item] start time of the initiating request, 
	[item] driver, 
	[item] the ip-address of the requestor, 
	[item] sent bytes,
	[item] received bytes,
	[item] the client data as provided via [lb]ns_conn
               clientdata[rb],
	[item] the cmd name of the callback, or "" when no callback is registered,
	[item] the callback condition flags, or "" when no callback is registered.
 [list_end]

[call [cmd "ns_connchan callback"] \
	[opt [option "-timeout [arg t]"]] \
	[opt [option "-receivetimeout [arg r]"]] \
	[opt [option "-sendtimeout [arg s]"]] \
	[arg channel] \
	[arg command] \
	[arg when] \
]

Register a Tcl callback for the names connection [arg channel].
[option -timeout] is the poll timeout, [option -receivetimeout] is a
timeout for incoming packets, [option -sendtimeout] is the timeout for
outgoing packets.  When [option -sendtimeout] has the value of 0, a
read operation might return the empty string. A value larger than 0
might block the event processing for the specified time.

[para]
The argument [arg when] consist of one or more characters
of r, w, e, or x, specifying, when the callback should fire.
All timeouts are specified in the form [arg secs[opt :microsecs]] or
[arg secs.fraction].

[para] When the callback is fired, the specified Tcl command will be
called with an additional argument, which is an indicator for the
reason of the call [arg when]. The value of [arg when] will be as
follows:

[list_begin itemized]
[item] r - the socket is readable
[item] w - the socket is writable
[item] e - the socket has an exceptional condition
[item] x - the server is shutting down
[item] t - timeout received
[list_end]

[para] When the callback exits, its return value determines, whether
the callback should be canceled or not. The return value is
interpreted as follows:
[list_begin itemized]
[item] 0 - the callback is canceled, and the channel is deleted
           automatically (typically, when an error occurs)
[item] 1 - the callback will be used as well for further events
[item] 2 - the callback will be suspended. No further events will
           be fired, but the channel is not deleted.
[list_end]


[call [cmd "ns_connchan listen"] \
	[opt [option "-driver [arg d]"]] \
	[opt [option "-server [arg s]"]] \
	[opt [option "-bind]"]] \
	[arg address] \
	[arg port] \
	[arg script] \
]

Open listening socket. Call the [arg script] callback on incoming
connections. On success, this command returns a dict containing
"channel", "port", "sock" and "address".


[call [cmd "ns_connchan open"] \
	[opt [option "-headers [arg h]"]] \
	[opt [option "-method [arg m]"]] \
	[opt [option "-timeout [arg t]"]] \
	[opt [option "-version [arg v]"]] \
	[arg url] \
]

Open a connection channel to the specified [arg url].
The URL might be an HTTP or an HTTPS URL.
[option -headers] refers to a [term ns_set] of request header fields,
[option -method] is the HTTP method (default GET),
[option -timeout] is the timeout for establishing the connection
(default 1 second), and
[option -version] specifies the HTTP version (default 1.0)

[call [cmd "ns_connchan read"] \
	[arg channel] \
]

Read from the specified connection channel.

[call [cmd "ns_connchan write"] \
	[arg channel] \
	[arg string] \	
]

Write to the specified connection channel. The function returns
the number of bytes sent, which might be less than the input length.

[list_end]

[see_also ns_conn ns_chan ns_sockcallback ns_write]
[keywords "server built-in" channels socket driver]

[manpage_end]

