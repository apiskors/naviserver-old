[include version_include.man]
[manpage_begin ns_shutdown n [vset version]]
[moddesc {NaviServer Built-in Commands}]

[titledesc {Shut down NaviServer}]

[description]

This command shuts down the server, optionally waiting [arg timeout]
seconds to let existing connections and background jobs finish.  When 
this time limit is exceeded the server shuts down immediately.

[para] When [arg timeout] is not specified the default or configured
[arg timeout] is used (default 20). The default can be changed by the
parameter [term shutdowntimeout] in the global server parameters
(section [term ns/parameters] in the config file). When [arg timeout]
it is specified, it must be an integer >= 0.

[section {COMMANDS}]

[list_begin definitions]

[call [cmd ns_shutdown] [opt [option -restart]] [opt [arg timeout]]]

[para]
[arg timeout]
Time in seconds to wait before shuting down the server

[para]
[option -restart]
send an interrupt signal to the server, leading to a non-zero exit code.

[list_end]

[section EXAMPLES]

[example_begin]
   ns_shutdown
   ns_shutdown 30
[example_end]


[see_also ns_kill]
[keywords "server built-in" restart]


[manpage_end]

