[include version_include.man]

[manpage_begin nssock n [vset version]]
[moddesc   {NaviServer Modules}]
[titledesc {Configuring HTTP socket communications}]

[description]

This module [emph nssock] is used for the socket communication over HTTP. For
the communication over HTTPS, the module [term nsssl] is used, which shares
the configuration parameters presented here and adds additional parameters.

[para]
The module [emph nssock] is typically loaded per server, e.g. for
[term server1]:

[example_begin]
   ns_section ns/server/server1/modules
   ns_param   nssock   nssock.so
 
   ns_section ns/server/server1/module/nssock
   ns_param port 8000
   ...
[example_end]

This amount of configuration is sufficient for many installations, which
do not want to run the server on port 80 (which requires as well root
permissions for starting).

[section CONFIGURATION]

The module [emph nssock] provides a rich set of configuration options, which
are important for large installation. However, all parameters are
equipped with sensible default values, such that none of these are required.

[list_begin definitions]

[def acceptsize]
Maximum number of requests accepted at once (integer, defaults to
the value [term backlog])

[def address] Space separated list of one or more IP addresses on
which the server should listen. If no address is given it obtain the
address from the host name (either the specified host name or the host
name reported by the OS).

[def backlog]
Maximum number of entries for waiting listen connections (integer,
default: 256)

[def bufsize]
Maximum size in bytes (octets) for a single receive operation.
The value can be specified in memory units (kB, MB, GB, KiB, MiB, GiB);
default: 16kB

[def closewait]
Timeout in seconds for close on socket to drain potential garbage if
no keep alive is performed. (integer, default: 2)

[def defaultserver] In a virtual server setup (when the driver module
is loaded globally), this parameter is required and refers to the
default server (the server, receiving all requests without host header
fields unspecified or with unspecified host header fields, not mapped
to a specific server)

[def deferaccept]
TCP performance option; use TCP_FASTOPEN or TCP_DEFER_ACCEPT or
SO_ACCEPTFILTER, whatever is available in your operating system to
improve the performance of the initial TCP 3-way handshake. The
capabilities of the operating system are determined at compile time.
If you want to use TCP_FASTOPEN (introduced by Linux kernel 3.7.0), 
you might have to activate this option in your kernel, 
by using [example {
   sudo sysctl net.ipv4.tcp_fastopen=1
}] (boolean, default: false)

[def driverthreads]
Number of driver threads threads. Specifying multiple driver threads
require the OS kernel to support SO_REUSEPORT and can improve the
performance for high load applications slightly. (integer, default: 1)

[para] When multiple driver threads are configured, parameter "reuseport" is
automatically set to "true".

[para] Note: When using multiple driver threads on privileged ports,
one has to add "#" followed by the number of necessary bind operations
to the prebind address. Otherwise, prebind will bind to the address
only once, and only one driverthread can be used.

[def extraheaders]
This parameter can be used to add extra response headers
for every response sent over this driver. The extraheaders
have the form for attribute/value pairs. This mechanism can be used
to implement for example HTTP Strict Transport Security in 
nsssl, which uses the same parameter definition.

[def hostname]
Hostname of the server, can be looked up automatically if not specified.

[def keepalivemaxdownloadsize]
Don't allow keep-alive for downloads content larger than this size in
bytes; a value of 0 means that this feature is deactivated.
The value can be specified in memory units (kB, MB, GB, KiB, MiB, GiB);
(default: 0)

[def keepalivemaxuploadsize]
Don't allow keep-alive for upload content larger than this size in
bytes; a value of 0 means that this feature is deactivated.
The value can be specified in memory units (kB, MB, GB, KiB, MiB, GiB);
(default: 0)

[def keepwait]
Timeout in seconds for keep-alive. (integer, default: 5)

[def location]
Provide a default for protocol://hostname[lb]:port[rb]. Useful
for virtual servers.

[def maxheaders]
Maximum number of header lines (integer, 128)

[def maxinput]
Maximum size in bytes (octets) for content loaded into the server,
such as upload of files.  The value can be specified in memory units
(kB, MB, GB, KiB, MiB, GiB); (default: 1MB)

[def maxline]
Maximum size in bytes (octets) of a single header line (integer,
default: 8192)

[def maxqueuesize]
Size of the queue of accepted requests waiting for a connection thread
(integer, default 1024).

[def maxupload] Spool uploads to tmp-filesb for uploads larger than
this size in bytes in order to keep the memory footprint small. This
is especially important when large uploads are allowed via [term maxinput]. If
this value is 0, this feature is deactivated. When spool-to-file is
activated, the minimal size is the size of [term readahead]
(see as well [term spoolerthreads]).
The value can be specified in memory units (kB, MB, GB, KiB, MiB, GiB);
(default: 0)

[def nodelay]
TCP Performance option; use TCP_NODELAY to disable Nagle algorithm
(boolean, default: true)

[def port]
Port, on which the server listens. When the port is specified as 0,
the module with its defined commands (such as [cmd ns_http] is
loaded, but the driver will not be listening on any port.
(integer, default: 80)


[def readahead]
Maximum size in bytes (octets) for asynchronous read ahead operations.
The value can be specified in memory units (kB, MB, GB, KiB, MiB, GiB);
(default: value of [term bufsize])

[def recvwait]
Timeout in seconds for receive operations. (integer, default: 30)

[def reuseport] When the parameter is set and SO_REUSEPORT is
supported by the os, NaviServer will allow multiple listen operations
on the same port (default: false)


[def sendwait]
Timeout in seconds for send operations. (integer, default: 30)

[def spoolerthreads]
Number of spooler threads used when content larger than
[term maxupload] is received. When spoolerthreads are set to 0, the driver
threads handles upload-spooling as wellto memory; (integer, default: 0)

[def uploadpath]
Directory for uploads, defaults to the setting of [term "ns/parameter tmpdir"].

[def writerbufsize]
Buffer size in bytes (octets) for writer threads.
The value can be specified in memory units (kB, MB, GB, KiB, MiB, GiB);
(default: 8kB)

[def writerratelimit]
Limit the rate of the data transferred via writer threads.
The value can be specified in KB/s (kilobytes per second) and
can be refined per connection pool or per single connection
(default: 0, meaning unlimited)

[def writersize]
Use writer threads for replies above this size.
The value can be specified in memory units (kB, MB, GB, KiB, MiB, GiB);
(default: 1MB)

[def writerstreaming]
Use writer threads for streaming HTML output
(e.g. ns_write ...). (boolean, default: false)

[def writerthreads]
Number of writer threads. (integer, default: 0)

[list_end]

[section EXAMPLES]

In the most simple case, one defines in a configuration file a single
server [emph s1] with single network driver [emph nssock]. In the
example below the server is listening on port 8000.

[example_begin]
   ns_section "ns/servers"
   ns_param    s1 "NaviServer Instance 1"

   ns_section "ns/server/s1/modules"
   ns_param    nssock        nssock.so

   ns_section "ns/server/s1/module/nssock"
   ns_param    address        0.0.0.0
   ns_param    port           8000
[example_end]

It is as well possible to define multiple servers in the same
configuration file (here [emph s1] and [emph s2]). These servers
use the same driver [emph nsock] but with different ports.
In this case it is sufficient to load the driver once.

[example_begin]
   ns_section "ns/servers"
   ns_param    s1 "NaviServer Instance 1"
   ns_param    s2 "NaviServer Instance 1"

   ns_section "ns/server/s1/modules"
   ns_param    nssock        nssock.so

   ns_section "ns/server/s1/module/nssock"
   ns_param    address        0.0.0.0
   ns_param    port           8000

   ns_section "ns/server/s2/modules"
   ns_param    nssock        nssock.so

   ns_section "ns/server/s2/module/nssock"
   ns_param    address        0.0.0.0
   ns_param    port           8001
[example_end]

When the config file above is named e.g. two-server-config.tcl,
the tow servers can be started with a command line like:

[example_begin]
/usr/local/ns/bin/nsd -u nsadmin -t two-server-config.tcl -f
[example_end]

When it is the goal to start only one of these servers, one can use
e.g. the following command:

[example_begin]
/usr/local/ns/bin/nsd -u nsadmin -t two-server-config.tcl -f -server s2
[example_end]

Similarly, we can define a single server, listening on multiple
ports. In this case, one can load multiple instances of the driver
where each of the driver listens on a different port. In the following
example we name the different instances of the network driver
[emph nssock1] and [emph nssock2].

[example_begin]
   ns_section ns/server/s1/modules
   ns_param   nssock1   nssock.so
   ns_param   nssock2   nssock.so
 
   ns_section ns/server/server1/module/nssock1
   ns_param   port    8000
   
   ns_section ns/server/server1/module/nssock2
   ns_param   port    8001
[example_end]

Now assume, we want to define two web servers "s1" and "s2", which
should act as virtual servers. This means, we want to define one
network driver, which listens on a single port, but which should
direct requests to the server based on the content of the [term host]
header field.  Using such virtual servers is a common technique, where
e.g. for the same IP address, multiple DNS names are
registered. According to HTTP/1.1, clients have to send the host name
in the host header field to the server, which can behave differently
depending on contents of this field.

[para]

Assume for the IP address of the server the DNS names [term foo.com],
[term bar.com] and [term baz.com] are registered. We define server
"s1" and "s2" such that "s1" should receive requests from
[term foo.com], and "s2" should receive requests from [term bar.com]
and [term baz.com]. Servers "s1" and "s2" have different
[term pagedir] definitions.

[para] For definging virtual servers, the drivers (e.g. [term nssock]
or [term nsssl] have to be loaded globally (i.e. under
[const ns/module/nssock].  For requests with missing/invalid host header
fields, we have to define a [term defaultserver] to handle such
requests in the global definition.

In the section [const ns/module/nssock/servers] we define the mapping
between the host names and the defined servers. Note that one can
define multiple DNS names also for a single server (here for [const s2]).

[example_begin]
   #
   # Define two servers
   #
   ns_section ns/servers
   ns_param   s1      "Virtual Server s1"
   ns_param   s2      "Virtual Server s2 "
   
   ns_section ns/server/s1/fastpath
   ns_param   pagedir  /var/www/s1
   
   ns_section ns/server/s2/fastpath
   ns_param   pagedir  /var/www/s2
   
   #
   # Define nssock driver, directing requests to the virtual servers
   #
   ns_section ns/modules
   ns_param   nssock   nssock.so
   
   ns_section ns/module/nssock
   ns_param   port     8000
   ns_param   defaultserver s1
   
   #
   # Define the mapping between the DNS names and the servers.
   #
   ns_section ns/module/nssock/servers
   ns_param   s1      foo.com
   ns_param   s2      bar.com
   ns_param   s2      baz.com
[example_end]


More to come here...


[see_also ns_log ns_write ns_http nsssl]

[keywords module nssock \
	performance tuning configuration \
	SO_REUSEPORT TCP TCP_FASTOPEN \
	nsssl driver prebind pagedir redirect]

[manpage_end]
