[include version_include.man]
[manpage_begin ns_writer n [vset version]]
[moddesc {NaviServer Built-in Commands}]

[titledesc {Manage files with writer thread}]

[description]

This command allows one to submit data or file to be returned to the
client connection using writer thread instead of current connection
thread. Writer thread processes multiple sockets using async socket
operations which consumes less resources than returning multiple files
to the slow clients in separate thread.

[section {COMMANDS}]

[list_begin definitions]

[call [cmd "ns_writer list"] \
	[opt [option {-server s}] ]]

[para] 
Returns list of all currently submitted files. Every list entry
contains the following elements:

[list_begin enumerated]
[enum] start time of the initiating request,
[enum] name of the thread,
[enum] driver,
[enum] pool,
[enum] IP address of the requestor,
[enum] file descriptor,
[enum] remaining size,
[enum] bytes already sent,
[enum] current transfer rate
[enum] rate limit, and
[enum] the client data as provided via [lb]ns_conn clientdata[rb].
[list_end]

If [option {-server}] is
specified, only the submitted file entries from the specified server
are returned.

[call [cmd ns_writer] [arg submit] [arg data]]

[para]
Submit arbitrary data to be returned via writer thread, data can be
binary. On success the function returns 1, otherwise 0.


[call [cmd "ns_writer submitfile"] \
	[opt [option -headers]] \
	[opt [option {-offset offset}]] \
	[opt [option {-size size}]] \
	[arg file]]

[para]
Submits file to be returned via writer thread. For partial requests,
ns_writer does NOT change status code or sets content-range. On
success the function returns 1, otherwise 0.
Optionally the following arguments can be used:

[para]
[option -headers]
Tells the writer to provide required HTTP headers, like content size,
type and status. When this option is not used, make sure that the
headers are provided (e.g. via [cmd ns_headers]) from your application.

[para]
[option -offset]
If specified, file will be returned starting with this offset.

[para]
[option -size]
If not specified, the whole file will be returned, otherwise only given
part. Can be combined with [option -offset].



[call [cmd "ns_writer size"] \
	[arg driver] \
	[opt [arg value]]]

[para]
Query or set configuration parameter [term writersize] of the writer(s) of
the specified driver.

[call [cmd "ns_writer streaming"] \
	[arg driver] \
	[opt [arg value]]]

[para]
Query or set configuration parameter [term writerstreaming] of the writer(s) of
the specified driver.


[list_end]

[section CONFIGURATION]
        
[para]
[emph {Basic configuration options:}]
        
[example_begin]
ns_section "ns/server/default/module/nssock" {
    ns_param writerthreads 1
    ns_param writersize 4kB
    ns_param writerstreaming true
}
[example_end]

[para]
[term writerthreads] parameter specified how many writer
threads will be used, multiple threads will be rotated to spread the
load across all threads

[para]
[term writersize] specifies the minimal size of the file to be
considered for writer thread, this parameter is used by connection
thread when returning file and if size exceeds configured value, file
will be returned via writer thread and connection thread will be
released to handle other requests.

[para]
[term writerstreaming] specifies whether or not streaming HTML
output (e.g. sent via [cmd ns_write]) should be sent via the writer.
When sending server responses over slow connections, this option
reduces the blocking time of the connection threads and can therefore
reduce the damage of slow-read attacks.

[section EXAMPLES]

[example_begin]
ns_writer submitfile /tmp/biglist.txt
[example_end]

[see_also ns_returnfile ns_conn ns_write ns_headers]
[keywords "server built-in" writer configuration]
[manpage_end]


